---
layout: page
title: 'Robert Wendt'
slug: '/'
path: '/'
---

---

**Email:** rwendt1337@gmail.com

**Gitlab:** https://gitlab.com/reedrichards

---

# Work Experience

<div style="display:flex;justify-content:space-between;flex-wrap:wrap;">

## SimpleCitizen

## _Backend Software Engineer_

</div>

_Oct. 2019 -  May. 2020_

- Lead a project to safely store and host development fixtures so devs
  could easily access latest development fixtures without keeping them
  in source control
  
- Added `pytest` to our codebase and created tests for permissions
  codebase and enabled org to perform large scale refactor of
  permissions codebase safely
  
- Provided mentorship and onboarded junior developers allowing them to
  grow to be better engineers and raise the ceiling for the
  engineering team as a whole
  
- Refactored various docker services to levarage caching and reduced
  build in half.
  
- Created contionus integration pipeline for `django` monolith which
  allowed org to confidently deploy code as engineering team scaled
  
- Integrated `graphene`'s test client with `django` which allowed devs
  to test with graphql endpoints which prevented regressions and 
  increased confidence in product deliverability
  

<div style="display:flex;justify-content:space-between;flex-wrap:wrap;">

## Peerfit

## _Backend Software Engineer_

</div>

_July. 2018 - Oct. 2019_


- Design asynchronous portal for our core API which allowed for us to
  integrate with third parties easier, allowing us to share important
  data with new business partners securely
  
- Created integration with Hubspot to allow marketing to identify
  users more specifically and create customized campaigns which
  increased our engagement
  
- Learned `django-treebeard` in one month to redesign our Django app
  to take on a insurance company leading to increasing our business
  customer base by 5x and doubling our user base
  

<div style="display:flex;justify-content:space-between;flex-wrap:wrap;">

## BVZZ Design

## _Freelance Web Development_

</div>

_Nov. 2017 - July 2018_

- Created websites and APIs using React.js and Django in order to
  allow business to have web presence and create transactions through
  there websites
  
- Deployed servers with nginx, gunicorn, and systemd, keeping business
  costs down
  
- Created E-mail server with postfix and dovecot to interact with
  customers and used Rainloop to interface the mail server with
  coworkers
  

<div style="display:flex;justify-content:space-between;flex-wrap:wrap;">

## Cook Financial Group

## _Web Development_

</div>

_Jan. 2017 - Nov. 2017_

- Created SEC compliant websites which allowed us to direct potential
  customers to sign up forms leading to 200 new customers
  
- Automated marketing E-mails and backup processes using Python which
  saved 24 hours a month and allowed us to maintain record keeping
  compliance
  

---



## Skills

<div style="display:flex;justify-content:space-between;flex-wrap:wrap;">

<div>

**Python**

- Django
- Django REST Framework
- Celery
- Graphene
- GraphQL
- Pytest

</div>

<div>

**Javascript**

- TypeScript
- NodeJS
- Express
- Gatsby
- React
- GraphQL

</div>

<div>

**DevOps**

- Kubernetes
- ELK Stack
- Gitlab CI/CD
- Docker
- Docker-Compose
- Nginx

</div>

</div>
